public class DataModel {
    public DataModel()
    {
        VisualStudioCodeScore = new ScoreModel();
        VisualStudioScore = new ScoreModel();
    }

    public ScoreModel VisualStudioScore { get; set; }
    public ScoreModel VisualStudioCodeScore { get; set; }
}