public class ScoreModel {
    public int PositiveVotes { get; set; }
    public int NegativeVotes { get; set; }
}
