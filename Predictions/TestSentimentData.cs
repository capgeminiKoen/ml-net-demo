﻿using System.Collections.Generic;

namespace binaryclassifier
{
    internal class TestSentimentData
    {
        internal static readonly IEnumerable<SentimentData> Sentiments = new[]
        {
            new SentimentData
            {
                SentimentText = "Contoso's 11 is a wonderful experience",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "The acting in this movie is very bad",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Joe versus the Volcano Coffee Company is a great film.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Visual studio sucks a lot.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Visual studio is horrible.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Visual studio code is really good.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "I love Visual studio code.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Visual studio code is perfect.",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Koen loves Visual studio code",
                Sentiment = 0
            },
            new SentimentData
            {
                SentimentText = "Hans loves visual studio",
                Sentiment = 0
            }
        };
    }
}