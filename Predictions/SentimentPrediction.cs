﻿using Microsoft.ML.Runtime.Api;

namespace binaryclassifier
{
    public class SentimentPrediction
    {
        [ColumnName("PredictedLabel")]
        public bool Sentiment;
    }
}