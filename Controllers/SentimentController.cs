using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using binaryclassifier.Predictions;

namespace binaryclassifier.Controllers
{

    [Produces("application/json")]
    [Route("api/sentiments")]
    public class SentimentController : Controller
    {

        private DataModel data = new DataModel();

        [HttpGet()]
        public DataModel Get()
        {
            return data;
        }

        [HttpPost("vscode")]
        public DataModel PostVsCodeString([FromBody] PostModel body)
        {
            if (body != null && PredictionHelper.TrainedModel != null)
            {
                Console.WriteLine("Received a description for VS code: {0}. Predicting the sentiment..", body.Text);
                var prediction = PredictionHelper.TrainedModel.Predict(new SentimentData()
                {
                    SentimentText = body.Text,
                    Sentiment = 0
                });

                Console.WriteLine("I think the prediction is {0}", prediction.Sentiment ? "positive" : "negative");

                if (prediction.Sentiment)
                {
                    data.VisualStudioCodeScore.PositiveVotes++;
                }
                else
                {
                    data.VisualStudioCodeScore.NegativeVotes++;
                }
            }
            return this.data;
        }

        [HttpPost("test")]
        public bool TestSentimentAnalysis([FromBody] PostModel body)
        {
            if (body != null && PredictionHelper.TrainedModel != null)
            {
                var prediction = PredictionHelper.TrainedModel.Predict(new SentimentData()
                {
                    SentimentText = body.Text,
                    Sentiment = 0
                });
                return prediction.Sentiment;
            }
            return false;
        }

        [HttpPost("vs")]
        public DataModel PostVsString([FromBody] PostModel body)
        {
            if (body != null && PredictionHelper.TrainedModel != null)
            {
                Console.WriteLine(@"Received a description for VS: {0} Predicting the sentiment..", body.Text);
                var prediction = PredictionHelper.TrainedModel.Predict(new SentimentData()
                {
                    SentimentText = body.Text,
                    Sentiment = 0
                });

                Console.WriteLine(@"I think the prediction is {0}", prediction.Sentiment ? "positive" : "negative");
                if (prediction.Sentiment)
                {
                    data.VisualStudioScore.PositiveVotes++;
                }
                else
                {
                    data.VisualStudioScore.NegativeVotes++;
                }
            }
            return this.data;
        }
    }
}