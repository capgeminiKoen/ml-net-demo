import { Component, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
    templateUrl: './sentiment-analysis.component.html',
    selector: 'app-sentiment-analysis',
    styleUrls: ['./styles.scss']
})
export class SentimentAnalysisComponent {

    public data: DataModel;
    public testText: string;
    public testTextResult?: boolean = null;
    public vsCodeText: string;
    public vsText: string;
    public loading: boolean = false;

    constructor(private readonly http: HttpClient, @Inject('BASE_URL') private readonly baseUrl: string) {
        this.loading = true;
        http.get<DataModel>(baseUrl + 'api/sentiments').subscribe(result => {
            this.data = result;
            this.loading = false;
        }, error => this.onError(error));
    }

    public submitTestText() {
        this.loading = true;
        this.http.post<boolean>(this.baseUrl + 'api/sentiments/test', {text: this.testText}).subscribe(result => {
            this.testTextResult = result;
            this.loading = false;
        }, error => this.onError(error));
    }

    public submitVsCodeText() {
        this.loading = true;
        this.http.post<DataModel>(this.baseUrl + 'api/sentiments/vscode', {text: this.vsCodeText}).subscribe(result => {
            this.data = result;
            this.vsCodeText = '';
            this.loading = false;
        }, error => this.onError(error));
    }

    public submitVsText() {
        this.loading = true;
        this.http.post<DataModel>(this.baseUrl + 'api/sentiments/vs', {text: this.vsText}).subscribe(result => {
            this.data = result;
            this.vsText = '';
            this.loading = false;
        }, error => this.onError(error));
    }

    private onError(error: any) {
        console.log(error);
        this.loading = false;
    }
}

interface DataModel {
    visualStudioScore: ScoreModel;
    visualStudioCodeScore: ScoreModel;
}

interface ScoreModel {
    positiveVotes: number;
    negativeVotes: number;
}